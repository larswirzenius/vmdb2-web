[[!meta title="vmdb2 can't build an image without partitions"]]
[[!tag ]]
[[!meta date="2020-03-31 08:39"]]

vmdb2 cannot currently build an image without partitions. Such an
image would sometimes be nice, and it'd be easier to resize. The
reason partitions are required is that vmdb2 specification files refer
to devices by symbolic names, or tags, and there's no way to refer to
the whole image with a tag.

This will eventually be rectified.
See <https://gitlab.com/larswirzenius/vmdb2/-/issues/21> if you want
to comment.
