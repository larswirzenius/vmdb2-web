[[!meta title="Development blog"]]

This is the **development** blog for vmdb2. It is meant to be a
communication channel for those interested in the development of vmdb2
itself.

This page collects all the blog entries in one page. Just the titles.

[[!inline pages="page(blog/*)" archive=yes trail=yes]]
