[[!meta title="Debian image builder"]]

[[About]] &mdash;
[[Contact]] &mdash;
[[Code|patches]] &mdash;
[[Release process|release]] &mdash;
[[Contributing]] &mdash;
[[Documentation]]

**Note:** vmdb2 is in "selfish maintenance mode". Lars maintains the
software to the extent he needs it, but is not spending time to
develop new features or debug problems he doesn't see himself. He will
review patches, however, so if you want vmdb2 to improve, make a
change and submit it for review.

vmdb2 installs a bare bones Debian system to a disk or disk image
file. It can be used for installing Debian to bare metal hardware,
bootable USB sticks, or to set up system disks for virtual machines.

Information on how we do things, and help to get you started if you're
interested:

* [[Code; also sending patches|patches]] or pull requests.
* Debian builds [images for Raspberry Pi](http://raspi.debian.net/)
  boards with vmdb2.

# Example .vmdb file

A .vmdb file defines how an image is built:

~~~yaml
steps:
  - mkimg: "{{ output }}"
    size: 4G

  - mklabel: msdos
    device: "{{ output }}"

  - mkpart: primary
    device: "{{ output }}"
    start: 1M
    end: 10M
    tag: unused

  - mkpart: primary
    device: "{{ output }}"
    start: 10M
    end: 100%
    tag: rootfs

  - kpartx: "{{ output }}"

  - mkfs: ext4
    partition: rootfs
    label: smoke

  - mount: rootfs

  - unpack-rootfs: rootfs

  - debootstrap: stretch
    mirror: http://deb.debian.org/debian
    target: rootfs
    unless: rootfs_unpacked

  - apt: install
    packages:
      - linux-image-amd64
    tag: rootfs
    unless: rootfs_unpacked

  - cache-rootfs: rootfs
    unless: rootfs_unpacked

  - chroot: rootfs
    shell: |
      sed -i '/^root:[^:]*:/s//root::/' /etc/passwd
      echo pc-vmdb2 > /etc/hostname

  - grub: bios
    tag: rootfs
    console: serial
~~~
